<?php

/*
 * 2. Kirjutage funktsioon is_in_list($list, $otsitav), mis ütleb kas listis
   on selline element või mitte.

   is_in_list([1, 2, 3], 2) === true;
   is_in_list([1, 2, 3], 4) === false;
*/

var_dump(is_in_list([1, 2, 3], 2) === true);
br();
var_dump(is_in_list([1, 2, 3], 4) === false);


function is_in_list($list, $otsitav)
{
    foreach ($list as $item) {
        if ($item === $otsitav) {
            return true;
        }
    }
    return false;
}