<?php

/*
 * 4. Kirjutage programm, mis käib läbi numbrid 1 kuni 15 ja väljastab järgmist:
   - kui number jagub kolmega, väljasta "Fizz";
   - kui number jagub viiega, väljasta "Buzz";
   - kui number jagub kolmega ja viiega, väljasta "FizzBuzz";
   - muudel juhtudel väljasta number ise.
 */

for ($i = 1; $i < 16; $i++) {
    if ($i % 3 === 0) {
        print('Fizz');
    }
    if ($i % 5 === 0) {
        print('Buzz');
    }
    if (!($i % 3 === 0) && !($i % 5 === 0)) {
        print($i);
    }
    print("<br>");
}