<?php

/*
 * 3. Kirjutage funktsioon, mis võtab argumendiks numbrite listi ja tagastab
   listi, milles on esialgse listi paaritud arvud.

   get_odd_numbers([1, 2, 3]) -> [1, 3]

   kontrollimaks, kas number on paaritu, saame kontrollida, kas kahega
   jagamisel jääb jääk. 4 % 2 === 0 ja 3 % 2 === 1.
*/

print_r(get_odd_numbers([1, 2, 3])); // [1, 3]
function get_odd_numbers($list)
{
    $result = [];
    foreach ($list as $item) {
        if ($item % 2 === 1) {
            array_push($result, $item);
        }
    }
    return $result;
}