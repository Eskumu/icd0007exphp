<?php

/*
 * 1. Kirjutage kood, mis loeb mitu korda esineb number (mitte sõne) 3
   $numbers listis ja väljastab tulemuse kujul "found it <mitu korda> times"
*/
$numbers = [1, 2, '3', 6, 2, 3, 2, 3];
$count = 0;

foreach ($numbers as $num) {
    if ($num === 3) {
        $count++;
    }
}

print("found it $count times");
