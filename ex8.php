<?php

/*
 * 8. Php-s on funktsioon rand($min, $max) mis tagastab juhusliku arvu
   antud piirides. Kirjutage kood, mis küsib 100000 juhuslikku arvu
   ühest viieni ja loeb kokku, mitu korda igat väärtust esines.

   Arvepidamiseks sobib hästi sõnastik. Võtmeks on arvud 1 - 5 ja
   väärtuseks see, mitu korda antud arv on esinenud.

   Seda, kas sõnastikus on mingi võti olemas saab kontrollida
   funktsiooniga isset($sõnastik[$võti]).
 */
$map = [];
for ($i=0; $i < 100000; $i++) {
    $num = rand(1,5);
    if (isset($map[$num])) {
        $map[$num] = ++$map[$num];
    } else {
        $map[$num] = 1;
    }
}
print_r($map);
print(array_sum($map));