<?php

/*
 * 5. Kirjutage programm, mis võtab sisendiks listi ja tagastab stringi,
   milles on kõik elemendid komaga eraldatuna ning seejärel teeb
   stringist esialgse listi tagasi.

   [1, 2, 3] -> "1, 2, 3" -> [1, 2, 3]
*/
$numbers = [3, 2, 5, 6];

$string = to_string($numbers);
$list = to_list($string);

print($string);
br();
print_r($list);


function to_string($list)
{
    return join(', ', $list);
}

function to_list($string)
{
    return explode(", ", $string);
}