<?php

/*
 * 6. Failis data/grades.txt on ained ja nendes saadud hinded. Tehke programm,
   mis loeb need andmed ja väljastab keskmise hinde.

   Stringi lõpust reavahetuse eemaldamiseks on funktsioon trim($string).
   ja listi elementide arvu ütleb funktsioon count($list).
*/

$file = 'data/grades.txt';

print_r(average_grade($file));


function file_to_map($file_location)
{
    $lines = file($file_location);
    $map = [];
    foreach ($lines as $line) {
        $line = explode(";", $line);
        $map[$line[0]] = $line[1];
    }
    return $map;
}

function average_grade($file_location)
{
    $map = file_to_map($file_location);
    return array_sum($map) / count($map);
}
