<?php
/**
 * Created by PhpStorm.
 * User: pro
 * Date: 28.02.2018
 * Time: 13:06
 */
require_once 'form.php';

$temp = $_GET['temp_in_celsius'];

print ($temp."C is ".to_fahrenheit($temp)."F");

function to_fahrenheit($temp) {
    return ($temp * 9/5) + 32;
}