<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="utf-8">
    <title>Php</title>
</head>
<body>
<p>
    Tänane kuupäev on
    <?php print
        (date
        (
            'Y-m-d'
        ));
    ?>.
</p>
<p>
    <?php
    $age = 17;
    $ageString = '17';
    // == doesn't check type
    //  === checks type
    if ($age === $ageString) {
        print('not true ');
    } else if ($age == $ageString) {
        print('if true ');
    } else {
        print("nope");
    }
    print('always');

    print('<p> Jill is $age old</p>');
    print("<p> Jill is $age old</p>");
    $result = "concatinate " . "strings " . "together";
    print ("<p>" . $result . "</p>");
    print ("<p>" . strtoupper($result) . "</p>");
    print ("<p>" . ucwords($result) . "</p>");
    print ("<p>" . bin2hex($result) . "</p>");

    $i = 0;
    while ($i < 3) {
        print($i . PHP_EOL);
        $i++;
    }

    $numbers = [1, 2, 3];
    array_push($numbers, 4);
    print("<p>");
    foreach ($numbers as $number) {
        print $number . PHP_EOL;
    }
    print("</p>");

    $numbers['greeting'] = 'hello';
    $numbers[9] = 9;
    print("<p>");
    print_r($numbers);
    print("</p>");

    $subject_grades
        = [
        'math'
        =>
            1
        ,
        'physics'
        =>
            4
    ];
    $subject_grades
    [
    'english'
    ] =
        4
    ;
    print_r
    (
        $subject_grades
    );

    ?>
</p>
<p>
    <?php
    foreach ($subject_grades as $key => $value) {
        print"$key grade is $value" . PHP_EOL;
    }
    ?>
</p>
<h2>Funktsioonid</h2>
<p>
    <?php
    print double(3);
    br();
    print fahrenheit(30);
    function double($x) {
        return $x + $x;
    }

    function fahrenheit($tempInCelsius) {
        return ($tempInCelsius * 9 / 5) + 32;
    }

    function br() {
        print("<br>");
        return;
    }
    ?>
</p>

<h2>Failid</h2>
<p>
    <?php
    file_put_contents('../data/results.txt', "some info\n");
    file_put_contents('../data/results.txt', "some other info", FILE_APPEND);
    $lines = file('../data/results.txt');
    print_r($lines);
    ?>
</p>

<h2>Join</h2>
<p>
    <?php
    $string = join(' + ', [1, 2, 3]);
    print $string;
    ?>

</p>

<h2>explode</h2>
<p>
    <?php
    $array = explode(', ', '1, 2, 3');
    print_r($array);
    ?>

</p>

<h2>require</h2>
<p>
<!--    --><?php
//    require '../Esimesed_proovid/index.php';
//    ?>

</p>

</body>
</html>